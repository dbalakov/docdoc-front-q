(function() {
	document.addEventListener("DOMContentLoaded", function() {
		var api  = new API();
		var view = new View(document.getElementById('input'), document.getElementById('output'));

		new Controller(view, api);
	});

	function API() {
		this.__cache = {};
	}

	API.prototype.get = function(url, onReady, onError) {
		if (this.__cache[url]) { return this.call(onReady, this.__cache[url]); }

		var request = new XMLHttpRequest();
		request.open('GET', url, true);
		request.send();

		request.onreadystatechange = function() {
		  if (request.readyState != 4) { return; }

			if (request.status === 200) {
				this.__cache[url] = request.responseText;
				return this.call(onReady, request.responseText);
			}
			this.call(onError, request.status);
		}.bind(this);
	};

	API.prototype.call = function(callback, argument) {
		if (!(callback instanceof Function)) { return; }
		callback(argument);
	};

	function View(input, output) {
		this.input  = input;
		this.output = output;

		this.__events = {};

		this.input.onchange = function() { this.dispatchEvent('inputChanged', this.getInput()); }.bind(this);
	}

	View.prototype.on = function(event, handler) {
		(this.__events[event] || (this.__events[event] = [])).push(handler);
	};

	View.prototype.dispatchEvent = function(event, argument) {
		(this.__events[event] || []).forEach(function(handler) { handler(argument); });
	};

	View.prototype.getInput  = function() { return input.value; };
	View.prototype.setOutput = function(value) { output.value = value; };

	function Controller(view, api) {
		this.view = view;
		this.api  = api;

		this.inputChanged = inputChanged.bind(this);

		this.view.on('inputChanged', this.inputChanged);
		this.inputChanged(view.getInput());

		function inputChanged(value) {
			this.view.setOutput(Controller.LOADING_MESSAGE);
			api.get(value, onReady.bind(this), onError.bind(this));

			function onReady(result) {
				if (this.view.getInput() != value) { return; }

				this.apiHandler(value, result);
			}

			function onError(status) {
				if (this.view.getInput() != value) { return; }

				this.view.setOutput(Controller.ERROR_MESSAGE);
			}
		}
	}

	Controller.prototype.apiHandler = function(url, result) {
		this.getApiHandler(url)(this, result);
	};

	Controller.prototype.getApiHandler = function(url) {
		var a = url.split('.');
		return Controller.API_HANDLERS[a[a.length - 1]] || Controller.DEFAULT_API_HANDLER;
	};

	Controller.API_HANDLERS = {
		js   : function(controller, result) { (new Function('cb', result))(function(value) { controller.view.setOutput(value.text); } ); },
		json : function(controller, result) { controller.view.setOutput(JSON.parse(result).text); }
	};

	Controller.DEFAULT_API_HANDLER = function(controller, result) { controller.view.setOutput(result); };

	Controller.ERROR_MESSAGE   = 'Произошла ошибка';
	Controller.LOADING_MESSAGE = 'Загрузка...';
})();
